﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TorneoJuegos.Models
{
    public class Tickets
    {
        [Key]
        public int IDTicket { get; set; }

        [Required]
        public String Ticket { get; set; }
    }
}