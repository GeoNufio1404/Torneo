﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TorneoJuegos.Models
{
    public class Participantes
    {

        [Key]
        public int IDParticipantes { get; set; }

        [Required]
        public String CodigoCarnet { get; set; }

        [Required]
        public String NombreCompleto { get; set; }

        [Required]
        public String Carrera { get; set; }

        [Required]
        public String Grado { get; set; }

        [Required]
        public String Seccion { get; set; }

        [Required]
        public String Jornada { get; set; }

        [ForeignKey("CodigoTecnico")]
        public int IDCodigoTecnico { get; set; }

        public virtual CodigoTecnico CodigoTecnico { get; set; }

        [ForeignKey("Tickets")]
        public int IDTicket { get; set; }

        public virtual Tickets Tickets { get; set; }

    }
}