﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TorneoJuegos.Models
{
    // Puede agregar datos del perfil del usuario agregando más propiedades a la clase ApplicationUser. Para más información, visite http://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {

        public virtual Torneo Torneo { get; set; }

        public virtual CodigoTecnico CodigoTecnico { get; set; }

        public virtual Juegos Juegos { get; set; }

        public virtual Tickets Tickets { get; set; }

        public virtual Participantes Participantes { get; set; }

        public virtual Contiendas Contiendas { get; set; }

        public virtual EstadoContiendas EstadoContiendas { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<TorneoJuegos.Models.CodigoTecnico> CodigoTecnicoes { get; set; }

        public System.Data.Entity.DbSet<TorneoJuegos.Models.Juegos> Juegos { get; set; }

        public System.Data.Entity.DbSet<TorneoJuegos.Models.Torneo> Torneos { get; set; }

        public System.Data.Entity.DbSet<TorneoJuegos.Models.Tickets> Tickets { get; set; }

        public System.Data.Entity.DbSet<TorneoJuegos.Models.Participantes> Participantes { get; set; }

        public System.Data.Entity.DbSet<TorneoJuegos.Models.EstadoContiendas> EstadoContiendas { get; set; }

        public System.Data.Entity.DbSet<TorneoJuegos.Models.Contiendas> Contiendas { get; set; }
    }
}