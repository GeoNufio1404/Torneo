﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TorneoJuegos.Models
{
    public class EstadoContiendas
    {
        [Key]
        public int IDEstadoContienda { get; set; }

        [Required]
        public string Estado { get; set; }
    }
}