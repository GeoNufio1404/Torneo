﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TorneoJuegos.Models
{
    public class Torneo
    {
        [Key]
        public int IDTorneo { get; set; }

        [Required]
        public String NombreTorneo { get; set; }

        [Required]
        public String Horario { get; set; }

        [Required]
        public String Salon { get; set; }

        [Required]
        public String Encargado { get; set; }

        [ForeignKey("Juegos")]
        public int IDJuego { get; set; }

        public virtual Juegos Juegos { get; set; }
    }
}