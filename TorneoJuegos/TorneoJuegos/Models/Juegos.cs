﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TorneoJuegos.Models
{
    public class Juegos
    {
        [Key]
        public int IDJuego { get; set; }

        [Required]
        public String Nombre { get; set; }
    }
}