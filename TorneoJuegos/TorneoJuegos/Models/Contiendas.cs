﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TorneoJuegos.Models
{
    public class Contiendas
    {

        [Key]
        public int IDContienda { get; set; }

        [ForeignKey("EstadoContiendas")]
        public int IDEstadoContienda { get; set; }
        public virtual EstadoContiendas EstadoContiendas { get; set; }

        [ForeignKey("Participantes")]
        public int IDParticipante { get; set; }
        public virtual Participantes Participantes { get; set; }

        [StringLength(maximumLength: 100)]
        public string Admin { get; set; }
    }
}