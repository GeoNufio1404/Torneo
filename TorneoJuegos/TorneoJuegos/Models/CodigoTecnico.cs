﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TorneoJuegos.Models
{
    public class CodigoTecnico
    {

        [Key]
        public int IDCodigoTecnico { get; set; }

        [Required]
        public String CodigoTec { get; set; }

        [Required]
        public String Carrera { get; set; }

        [Required]
        public String Grado { get; set; }

        [Required]
        public String Seccion { get; set; }

        [Required]
        public String Jornada { get; set; }

           
    }
}