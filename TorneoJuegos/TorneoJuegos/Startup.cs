﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TorneoJuegos.Startup))]
namespace TorneoJuegos
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
