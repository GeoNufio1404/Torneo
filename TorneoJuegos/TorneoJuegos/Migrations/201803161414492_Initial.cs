namespace TorneoJuegos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CodigoTecnicoes",
                c => new
                    {
                        IDCodigoTecnico = c.Int(nullable: false, identity: true),
                        CodigoTec = c.String(nullable: false),
                        Carrera = c.String(nullable: false),
                        Grado = c.String(nullable: false),
                        Seccion = c.String(nullable: false),
                        Jornada = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IDCodigoTecnico);
            
            CreateTable(
                "dbo.Contiendas",
                c => new
                    {
                        IDContienda = c.Int(nullable: false, identity: true),
                        IDEstadoContienda = c.Int(nullable: false),
                        IDParticipante = c.Int(nullable: false),
                        Admin = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.IDContienda)
                .ForeignKey("dbo.EstadoContiendas", t => t.IDEstadoContienda, cascadeDelete: true)
                .ForeignKey("dbo.Participantes", t => t.IDParticipante, cascadeDelete: true)
                .Index(t => t.IDEstadoContienda)
                .Index(t => t.IDParticipante);
            
            CreateTable(
                "dbo.EstadoContiendas",
                c => new
                    {
                        IDEstadoContienda = c.Int(nullable: false, identity: true),
                        Estado = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IDEstadoContienda);
            
            CreateTable(
                "dbo.Participantes",
                c => new
                    {
                        IDParticipantes = c.Int(nullable: false, identity: true),
                        CodigoCarnet = c.String(nullable: false),
                        NombreCompleto = c.String(nullable: false),
                        Carrera = c.String(nullable: false),
                        Grado = c.String(nullable: false),
                        Seccion = c.String(nullable: false),
                        Jornada = c.String(nullable: false),
                        IDCodigoTecnico = c.Int(nullable: false),
                        IDTicket = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IDParticipantes)
                .ForeignKey("dbo.CodigoTecnicoes", t => t.IDCodigoTecnico, cascadeDelete: true)
                .ForeignKey("dbo.Tickets", t => t.IDTicket, cascadeDelete: true)
                .Index(t => t.IDCodigoTecnico)
                .Index(t => t.IDTicket);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        IDTicket = c.Int(nullable: false, identity: true),
                        Ticket = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IDTicket);
            
            CreateTable(
                "dbo.Juegos",
                c => new
                    {
                        IDJuego = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IDJuego);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Torneos",
                c => new
                    {
                        IDTorneo = c.Int(nullable: false, identity: true),
                        NombreTorneo = c.String(nullable: false),
                        Horario = c.String(nullable: false),
                        Salon = c.String(nullable: false),
                        Encargado = c.String(nullable: false),
                        IDJuego = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IDTorneo)
                .ForeignKey("dbo.Juegos", t => t.IDJuego, cascadeDelete: true)
                .Index(t => t.IDJuego);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        CodigoTecnico_IDCodigoTecnico = c.Int(),
                        Contiendas_IDContienda = c.Int(),
                        EstadoContiendas_IDEstadoContienda = c.Int(),
                        Juegos_IDJuego = c.Int(),
                        Participantes_IDParticipantes = c.Int(),
                        Tickets_IDTicket = c.Int(),
                        Torneo_IDTorneo = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CodigoTecnicoes", t => t.CodigoTecnico_IDCodigoTecnico)
                .ForeignKey("dbo.Contiendas", t => t.Contiendas_IDContienda)
                .ForeignKey("dbo.EstadoContiendas", t => t.EstadoContiendas_IDEstadoContienda)
                .ForeignKey("dbo.Juegos", t => t.Juegos_IDJuego)
                .ForeignKey("dbo.Participantes", t => t.Participantes_IDParticipantes)
                .ForeignKey("dbo.Tickets", t => t.Tickets_IDTicket)
                .ForeignKey("dbo.Torneos", t => t.Torneo_IDTorneo)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.CodigoTecnico_IDCodigoTecnico)
                .Index(t => t.Contiendas_IDContienda)
                .Index(t => t.EstadoContiendas_IDEstadoContienda)
                .Index(t => t.Juegos_IDJuego)
                .Index(t => t.Participantes_IDParticipantes)
                .Index(t => t.Tickets_IDTicket)
                .Index(t => t.Torneo_IDTorneo);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Torneo_IDTorneo", "dbo.Torneos");
            DropForeignKey("dbo.AspNetUsers", "Tickets_IDTicket", "dbo.Tickets");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "Participantes_IDParticipantes", "dbo.Participantes");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "Juegos_IDJuego", "dbo.Juegos");
            DropForeignKey("dbo.AspNetUsers", "EstadoContiendas_IDEstadoContienda", "dbo.EstadoContiendas");
            DropForeignKey("dbo.AspNetUsers", "Contiendas_IDContienda", "dbo.Contiendas");
            DropForeignKey("dbo.AspNetUsers", "CodigoTecnico_IDCodigoTecnico", "dbo.CodigoTecnicoes");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Torneos", "IDJuego", "dbo.Juegos");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Contiendas", "IDParticipante", "dbo.Participantes");
            DropForeignKey("dbo.Participantes", "IDTicket", "dbo.Tickets");
            DropForeignKey("dbo.Participantes", "IDCodigoTecnico", "dbo.CodigoTecnicoes");
            DropForeignKey("dbo.Contiendas", "IDEstadoContienda", "dbo.EstadoContiendas");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", new[] { "Torneo_IDTorneo" });
            DropIndex("dbo.AspNetUsers", new[] { "Tickets_IDTicket" });
            DropIndex("dbo.AspNetUsers", new[] { "Participantes_IDParticipantes" });
            DropIndex("dbo.AspNetUsers", new[] { "Juegos_IDJuego" });
            DropIndex("dbo.AspNetUsers", new[] { "EstadoContiendas_IDEstadoContienda" });
            DropIndex("dbo.AspNetUsers", new[] { "Contiendas_IDContienda" });
            DropIndex("dbo.AspNetUsers", new[] { "CodigoTecnico_IDCodigoTecnico" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Torneos", new[] { "IDJuego" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Participantes", new[] { "IDTicket" });
            DropIndex("dbo.Participantes", new[] { "IDCodigoTecnico" });
            DropIndex("dbo.Contiendas", new[] { "IDParticipante" });
            DropIndex("dbo.Contiendas", new[] { "IDEstadoContienda" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Torneos");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Juegos");
            DropTable("dbo.Tickets");
            DropTable("dbo.Participantes");
            DropTable("dbo.EstadoContiendas");
            DropTable("dbo.Contiendas");
            DropTable("dbo.CodigoTecnicoes");
        }
    }
}
