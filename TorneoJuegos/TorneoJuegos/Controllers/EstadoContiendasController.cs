﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TorneoJuegos.Models;

namespace TorneoJuegos.Controllers
{
    public class EstadoContiendasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: EstadoContiendas
        public async Task<ActionResult> Index()
        {
            return View(await db.EstadoContiendas.ToListAsync());
        }

        // GET: EstadoContiendas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoContiendas estadoContiendas = await db.EstadoContiendas.FindAsync(id);
            if (estadoContiendas == null)
            {
                return HttpNotFound();
            }
            return View(estadoContiendas);
        }

        // GET: EstadoContiendas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EstadoContiendas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDEstadoContienda,Estado")] EstadoContiendas estadoContiendas)
        {
            if (ModelState.IsValid)
            {
                db.EstadoContiendas.Add(estadoContiendas);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(estadoContiendas);
        }

        // GET: EstadoContiendas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoContiendas estadoContiendas = await db.EstadoContiendas.FindAsync(id);
            if (estadoContiendas == null)
            {
                return HttpNotFound();
            }
            return View(estadoContiendas);
        }

        // POST: EstadoContiendas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDEstadoContienda,Estado")] EstadoContiendas estadoContiendas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estadoContiendas).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(estadoContiendas);
        }

        // GET: EstadoContiendas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoContiendas estadoContiendas = await db.EstadoContiendas.FindAsync(id);
            if (estadoContiendas == null)
            {
                return HttpNotFound();
            }
            return View(estadoContiendas);
        }

        // POST: EstadoContiendas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            EstadoContiendas estadoContiendas = await db.EstadoContiendas.FindAsync(id);
            db.EstadoContiendas.Remove(estadoContiendas);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
