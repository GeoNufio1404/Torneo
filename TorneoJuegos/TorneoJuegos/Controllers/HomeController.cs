﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TorneoJuegos.Models;

namespace TorneoJuegos.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Torneos()
        {
            ViewBag.Message = "Torneos";

            return View();
        }

        public ActionResult Participantes()
        {
            ViewBag.Message = "Participantes";

                return View();
        }

        public ActionResult Ticket()
        {
            ViewBag.Message = "Ticket";

            return View();
        }

        [HttpPost]
        public ActionResult VerificaTicket(Tickets objeto)
        {
            if (ModelState.IsValid)
            {
                ApplicationDbContext db = new ApplicationDbContext();
                string Ticket = objeto.Ticket;
                var consulta = from u in db.Tickets where u.Ticket == Ticket select u;


                if (consulta.Count() > 0)
                {
                    return View("VerificaTicket");
                }
                else
                {

                    return View("Ticket");
                }
            }
            else
            {
                return View("Home");
            }


        }
    }
}