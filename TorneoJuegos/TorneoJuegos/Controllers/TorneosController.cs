﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TorneoJuegos.Models;

namespace TorneoJuegos.Controllers
{
    public class TorneosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Torneos
        public async Task<ActionResult> Index()
        {
            var torneos = db.Torneos.Include(t => t.Juegos);
            return View(await torneos.ToListAsync());
        }

        // GET: Torneos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Torneo torneo = await db.Torneos.FindAsync(id);
            if (torneo == null)
            {
                return HttpNotFound();
            }
            return View(torneo);
        }

        // GET: Torneos/Create
        public ActionResult Create()
        {
            ViewBag.IDJuego = new SelectList(db.Juegos, "IDJuego", "Nombre");
            return View();
        }

        // POST: Torneos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDTorneo,NombreTorneo,Horario,Salon,Encargado,IDJuego")] Torneo torneo)
        {
            if (ModelState.IsValid)
            {
                db.Torneos.Add(torneo);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IDJuego = new SelectList(db.Juegos, "IDJuego", "Nombre", torneo.IDJuego);
            return View(torneo);
        }

        // GET: Torneos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Torneo torneo = await db.Torneos.FindAsync(id);
            if (torneo == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDJuego = new SelectList(db.Juegos, "IDJuego", "Nombre", torneo.IDJuego);
            return View(torneo);
        }

        // POST: Torneos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDTorneo,NombreTorneo,Horario,Salon,Encargado,IDJuego")] Torneo torneo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(torneo).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IDJuego = new SelectList(db.Juegos, "IDJuego", "Nombre", torneo.IDJuego);
            return View(torneo);
        }

        // GET: Torneos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Torneo torneo = await db.Torneos.FindAsync(id);
            if (torneo == null)
            {
                return HttpNotFound();
            }
            return View(torneo);
        }

        // POST: Torneos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Torneo torneo = await db.Torneos.FindAsync(id);
            db.Torneos.Remove(torneo);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
