﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TorneoJuegos.Models;

namespace TorneoJuegos.Controllers
{
    public class ParticipantesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Participantes
        public async Task<ActionResult> Index()
        {
            var participantes = db.Participantes.Include(p => p.CodigoTecnico).Include(p => p.Tickets);
            return View(await participantes.ToListAsync());
        }

        // GET: Participantes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Participantes participantes = await db.Participantes.FindAsync(id);
            if (participantes == null)
            {
                return HttpNotFound();
            }
            return View(participantes);
        }

        // GET: Participantes/Create
        public ActionResult Create()
        {
            ViewBag.IDCodigoTecnico = new SelectList(db.CodigoTecnicoes, "IDCodigoTecnico", "CodigoTec");
            ViewBag.IDTicket = new SelectList(db.Tickets, "IDTicket", "Ticket");
            return View();
        }

        // POST: Participantes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDParticipantes,CodigoCarnet,NombreCompleto,Carrera,Grado,Seccion,Jornada,IDCodigoTecnico,IDTicket")] Participantes participantes)
        {
            if (ModelState.IsValid)
            {
                db.Participantes.Add(participantes);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IDCodigoTecnico = new SelectList(db.CodigoTecnicoes, "IDCodigoTecnico", "CodigoTec", participantes.IDCodigoTecnico);
            ViewBag.IDTicket = new SelectList(db.Tickets, "IDTicket", "Ticket", participantes.IDTicket);
            return View(participantes);
        }

        // GET: Participantes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Participantes participantes = await db.Participantes.FindAsync(id);
            if (participantes == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDCodigoTecnico = new SelectList(db.CodigoTecnicoes, "IDCodigoTecnico", "CodigoTec", participantes.IDCodigoTecnico);
            ViewBag.IDTicket = new SelectList(db.Tickets, "IDTicket", "Ticket", participantes.IDTicket);
            return View(participantes);
        }

        // POST: Participantes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDParticipantes,CodigoCarnet,NombreCompleto,Carrera,Grado,Seccion,Jornada,IDCodigoTecnico,IDTicket")] Participantes participantes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(participantes).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IDCodigoTecnico = new SelectList(db.CodigoTecnicoes, "IDCodigoTecnico", "CodigoTec", participantes.IDCodigoTecnico);
            ViewBag.IDTicket = new SelectList(db.Tickets, "IDTicket", "Ticket", participantes.IDTicket);
            return View(participantes);
        }

        // GET: Participantes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Participantes participantes = await db.Participantes.FindAsync(id);
            if (participantes == null)
            {
                return HttpNotFound();
            }
            return View(participantes);
        }

        // POST: Participantes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Participantes participantes = await db.Participantes.FindAsync(id);
            db.Participantes.Remove(participantes);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
