﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TorneoJuegos.Models;
using Microsoft.AspNet.Identity;

namespace TorneoJuegos.Controllers
{
    public class ContiendasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Contiendas
        public async Task<ActionResult> Index()
        {
            var contiendas = db.Contiendas.Include(c => c.EstadoContiendas).Include(c => c.Participantes);
            return View(await contiendas.ToListAsync());
        }

        // GET: Contiendas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contiendas contiendas = await db.Contiendas.FindAsync(id);
            if (contiendas == null)
            {
                return HttpNotFound();
            }
            return View(contiendas);
        }

        // GET: Contiendas/Create
        public ActionResult Create()
        {
            ViewBag.IDEstadoContienda = new SelectList(db.EstadoContiendas, "IDEstadoContienda", "Estado");
            ViewBag.IDParticipante = new SelectList(db.Participantes, "IDParticipantes", "CodigoCarnet");
            return View();
        }

        // POST: Contiendas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDContienda,IDEstadoContienda,IDParticipante")] Contiendas contiendas)
        {
            if (ModelState.IsValid)
            {
                contiendas.Admin = User.Identity.GetUserName();
                db.Contiendas.Add(contiendas);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IDEstadoContienda = new SelectList(db.EstadoContiendas, "IDEstadoContienda", "Estado", contiendas.IDEstadoContienda);
            ViewBag.IDParticipante = new SelectList(db.Participantes, "IDParticipantes", "CodigoCarnet", contiendas.IDParticipante);
            return View(contiendas);
        }

        // GET: Contiendas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contiendas contiendas = await db.Contiendas.FindAsync(id);
            if (contiendas == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDEstadoContienda = new SelectList(db.EstadoContiendas, "IDEstadoContienda", "Estado", contiendas.IDEstadoContienda);
            ViewBag.IDParticipante = new SelectList(db.Participantes, "IDParticipantes", "CodigoCarnet", contiendas.IDParticipante);
            return View(contiendas);
        }

        // POST: Contiendas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDContienda,IDEstadoContienda,IDParticipante")] Contiendas contiendas)
        {
            if (ModelState.IsValid)
            {
                contiendas.Admin = User.Identity.GetUserName();
                db.Entry(contiendas).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IDEstadoContienda = new SelectList(db.EstadoContiendas, "IDEstadoContienda", "Estado", contiendas.IDEstadoContienda);
            ViewBag.IDParticipante = new SelectList(db.Participantes, "IDParticipantes", "CodigoCarnet", contiendas.IDParticipante);
            return View(contiendas);
        }

        // GET: Contiendas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contiendas contiendas = await db.Contiendas.FindAsync(id);
            if (contiendas == null)
            {
                return HttpNotFound();
            }
            return View(contiendas);
        }

        // POST: Contiendas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Contiendas contiendas = await db.Contiendas.FindAsync(id);
            db.Contiendas.Remove(contiendas);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
