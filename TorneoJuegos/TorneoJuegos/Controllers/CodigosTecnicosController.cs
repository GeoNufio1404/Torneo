﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TorneoJuegos.Models;

namespace TorneoJuegos.Controllers
{
    public class CodigosTecnicosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: CodigosTecnicos
        public async Task<ActionResult> Index()
        {
            return View(await db.CodigoTecnicoes.ToListAsync());
        }

        // GET: CodigosTecnicos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CodigoTecnico codigoTecnico = await db.CodigoTecnicoes.FindAsync(id);
            if (codigoTecnico == null)
            {
                return HttpNotFound();
            }
            return View(codigoTecnico);
        }

        // GET: CodigosTecnicos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CodigosTecnicos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDCodigoTecnico,CodigoTec,Carrera,Grado,Seccion,Jornada")] CodigoTecnico codigoTecnico)
        {
            if (ModelState.IsValid)
            {
                db.CodigoTecnicoes.Add(codigoTecnico);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(codigoTecnico);
        }

        // GET: CodigosTecnicos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CodigoTecnico codigoTecnico = await db.CodigoTecnicoes.FindAsync(id);
            if (codigoTecnico == null)
            {
                return HttpNotFound();
            }
            return View(codigoTecnico);
        }

        // POST: CodigosTecnicos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDCodigoTecnico,CodigoTec,Carrera,Grado,Seccion,Jornada")] CodigoTecnico codigoTecnico)
        {
            if (ModelState.IsValid)
            {
                db.Entry(codigoTecnico).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(codigoTecnico);
        }

        // GET: CodigosTecnicos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CodigoTecnico codigoTecnico = await db.CodigoTecnicoes.FindAsync(id);
            if (codigoTecnico == null)
            {
                return HttpNotFound();
            }
            return View(codigoTecnico);
        }

        // POST: CodigosTecnicos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            CodigoTecnico codigoTecnico = await db.CodigoTecnicoes.FindAsync(id);
            db.CodigoTecnicoes.Remove(codigoTecnico);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
